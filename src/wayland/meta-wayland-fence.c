/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */

/*
 * Copyright (C) 2017 Intel Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Written by:
 *     Daniel Stone <daniels@collabora.com>
 */

#include "config.h"

#include "wayland/meta-wayland-fence.h"

#include "cogl/cogl.h"
#include "cogl/cogl-egl.h"
#include "backends/meta-backend-private.h"
#include "backends/meta-egl.h"
#include "backends/meta-egl-ext.h"
#include "meta/meta-backend.h"
#include "wayland/meta-wayland-private.h"
#include "wayland/meta-wayland-versions.h"

#include "linux-explicit-synchronization-unstable-v1-server-protocol.h"

/*
 * A ref-counted FD, used to share a fence created by the compositor from its
 * own rendering, which will later be passed to clients in a release
 * event.
 */
struct _MetaWaylandReleaseFence
{
  GObject parent;

  int fence_fd;
};

G_DEFINE_TYPE (MetaWaylandReleaseFence, meta_wayland_release_fence, G_TYPE_OBJECT);

/*
 * Represents a client in/release fence pair.
 */
struct _MetaWaylandSurfaceFence
{
  GObject parent;

  struct wl_resource *release; /* zwp_buffer_release_v1 */
  struct wl_resource *surface_sync; /* zwp_surface_synchronization_v1 */

  EGLSyncKHR in_fence;
  MetaWaylandReleaseFence *last_release;

  struct wl_list link; /* MetaWaylandCompositor::release_fences */
};

G_DEFINE_TYPE (MetaWaylandSurfaceFence, meta_wayland_surface_fence, G_TYPE_OBJECT);

static void
buffer_release_destroy(struct wl_client   *client,
                       struct wl_resource *resource)
{
  wl_resource_destroy (resource);
}

static void
buffer_release_destructor (struct wl_resource *resource)
{
  MetaWaylandSurfaceFence *surface_fence = wl_resource_get_user_data (resource);

  surface_fence->release = NULL;
  g_object_unref (surface_fence);
}

static const struct zwp_buffer_release_v1_interface release_implementation =
{
  buffer_release_destroy
};

static void
surface_fence_destroy(struct wl_client   *client,
                      struct wl_resource *resource)
{
  wl_resource_destroy (resource);
}

static void
surface_fence_destructor (struct wl_resource *resource)
{
  MetaWaylandSurface *surface = wl_resource_get_user_data (resource);

  if (surface->pending->synchronization)
    g_clear_object (&surface->pending->synchronization);

  if (surface->synchronization &&
      surface->synchronization->surface_sync == resource)
    g_clear_object (&surface->synchronization);

  surface->surface_synchronization = NULL;
}

void
meta_wayland_surface_fence_release (MetaWaylandSurface *surface)
{
  MetaWaylandSurfaceFence *fence = surface->synchronization;

  if (!fence || !fence->release)
    return;

  if (fence->last_release)
    {
      zwp_buffer_release_v1_send_fenced_release (fence->release,
                                                 fence->last_release->fence_fd);
      g_clear_object (&fence->last_release);
    }
  else
    {
      zwp_buffer_release_v1_send_immediate_release (fence->release);
    }
}

gboolean
meta_wayland_surface_add_release_fence (MetaWaylandSurface *surface,
                                        struct wl_list *list)
{
  if (!surface->synchronization || !surface->synchronization->release)
    return FALSE;

  wl_list_insert (list, &surface->synchronization->link);
  return TRUE;
}

void
meta_wayland_surface_set_release_fence (struct wl_list          *list,
                                        MetaWaylandReleaseFence *release)
{
  MetaWaylandSurfaceFence *surface_fence;

  surface_fence = wl_container_of (list, surface_fence, link);

  if (!surface_fence->release)
    return;

  if (surface_fence->last_release)
    g_clear_object (&surface_fence->last_release);

  if (release)
    surface_fence->last_release = g_object_ref (release);
}

static EGLSync
create_fence_from_fd (int fence_fd, GError **error)
{
  MetaBackend *backend = meta_get_backend ();
  MetaEgl *egl = meta_backend_get_egl (backend);
  ClutterBackend *clutter_backend = meta_backend_get_clutter_backend (backend);
  CoglContext *cogl_context = clutter_backend_get_cogl_context (clutter_backend);
  EGLDisplay egl_display = cogl_egl_context_get_egl_display (cogl_context);
  EGLint attribs[] = {
    EGL_SYNC_NATIVE_FENCE_FD_ANDROID, fence_fd,
    EGL_NONE,
  };

  return meta_egl_create_sync (egl, egl_display,
                               EGL_SYNC_NATIVE_FENCE_ANDROID, attribs,
                               error);
}

MetaWaylandReleaseFence *
meta_wayland_release_fence_get ()
{
  MetaBackend *backend = meta_get_backend ();
  MetaEgl *egl = meta_backend_get_egl (backend);
  ClutterBackend *clutter_backend = meta_backend_get_clutter_backend (backend);
  CoglContext *cogl_context = clutter_backend_get_cogl_context (clutter_backend);
  EGLDisplay egl_display = cogl_egl_context_get_egl_display (cogl_context);
  GError *error = NULL;
  MetaWaylandReleaseFence *release;
  EGLSync egl_fence;
  int fd;

  egl_fence = create_fence_from_fd (EGL_NO_NATIVE_FENCE_FD_ANDROID, &error);
  if (!egl_fence)
    {
      g_warning ("Failed to create EGLSync for out-fence: %s",
                 error ? error->message : "unknown error");
      return NULL;
    }

  fd = meta_egl_dup_native_fence_fd (egl, egl_display, egl_fence, &error);
  meta_egl_destroy_sync (egl, egl_display, egl_fence, NULL);
  if (fd < 0)
    {
      g_warning ("Failed to duplicate out-fence FD: %s",
                 error ? error->message : "unknown error");
      return NULL;
    }

  release = g_object_new (META_TYPE_WAYLAND_RELEASE_FENCE, NULL);
  release->fence_fd = fd;

  return release;
}

void
meta_wayland_surface_fence_wait (MetaWaylandSurface *surface)
{
  MetaBackend *backend = meta_get_backend ();
  MetaEgl *egl = meta_backend_get_egl (backend);
  ClutterBackend *clutter_backend = meta_backend_get_clutter_backend (backend);
  CoglContext *cogl_context = clutter_backend_get_cogl_context (clutter_backend);
  EGLDisplay egl_display = cogl_egl_context_get_egl_display (cogl_context);
  GError *error = NULL;

  if (!surface->synchronization || !surface->synchronization->in_fence)
    return;

  if (!meta_egl_wait_sync (egl,
                           egl_display,
                           surface->synchronization->in_fence,
                           0,
                           &error))
    {
      g_warning ("Could not wait on client in-fence: %s",
                 error ? error->message : NULL);
      g_error_free (error);
    }
}

static void
surface_fence_set_acquire_fence(struct wl_client   *client,
                                struct wl_resource *resource,
                                int                 fence_fd)
{
  MetaWaylandSurface *surface = wl_resource_get_user_data (resource);
  GError *error = NULL;

  if (!surface->pending->synchronization)
    {
      surface->pending->synchronization =
        g_object_new (META_TYPE_WAYLAND_SURFACE_FENCE, NULL);
      surface->pending->synchronization->surface_sync = resource;
    }

  if (surface->pending->synchronization->in_fence != NULL)
    {
      wl_resource_post_error (resource,
                              ZWP_SURFACE_SYNCHRONIZATION_V1_ERROR_DUPLICATE_FENCE,
                              "fence already attached to this surface commit");
      close (fence_fd);
      return;
    }

  surface->pending->synchronization->in_fence =
    create_fence_from_fd (fence_fd, &error);
  if (!surface->pending->synchronization->in_fence)
    {
      wl_resource_post_error (resource,
                              ZWP_SURFACE_SYNCHRONIZATION_V1_ERROR_INVALID_FENCE,
                              "could not import in-fence: %s",
                              error ? error->message : "unknown error");
      g_error_free (error);
      close (fence_fd);
      return;
    }

  close (fence_fd);
}

static void
surface_fence_get_release(struct wl_client   *client,
                          struct wl_resource *resource,
                          uint32_t            new_id)
{
  MetaWaylandSurface *surface = wl_resource_get_user_data (resource);

  if (!surface->pending->synchronization)
    {
      surface->pending->synchronization =
        g_object_new (META_TYPE_WAYLAND_SURFACE_FENCE, NULL);
      surface->pending->synchronization->surface_sync = resource;
    }

  if (surface->pending->synchronization->release)
    {
      wl_resource_post_error (resource,
                              ZWP_SURFACE_SYNCHRONIZATION_V1_ERROR_DUPLICATE_RELEASE,
                              "release object already exists to this surface commit");
      return;
    }

  surface->pending->synchronization->release =
    wl_resource_create (client,
                        &zwp_buffer_release_v1_interface,
                        wl_resource_get_version (resource),
                        new_id);
  wl_resource_set_implementation (surface->pending->synchronization->release,
                                  &release_implementation,
                                  g_object_ref (surface->pending->synchronization),
                                  buffer_release_destructor);
}

static const struct zwp_surface_synchronization_v1_interface surface_fence_implementation =
{
  surface_fence_destroy,
  surface_fence_set_acquire_fence,
  surface_fence_get_release
};

static void
factory_destroy(struct wl_client   *client,
                struct wl_resource *resource)
{
  wl_resource_destroy (resource);
}

static void
factory_get_synchronization(struct wl_client   *client,
                            struct wl_resource *factory_resource,
                            uint32_t            new_id,
                            struct wl_resource *surface_resource)
{
  MetaWaylandSurface *surface = wl_resource_get_user_data (surface_resource);

  if (surface->surface_synchronization)
    {
      wl_resource_post_error (factory_resource,
                              ZWP_LINUX_EXPLICIT_SYNCHRONIZATION_V1_ERROR_SYNCHRONIZATION_EXISTS,
                              "surface already has a synchronization object");
      return;
    }

  surface->surface_synchronization =
    wl_resource_create (client,
                        &zwp_surface_synchronization_v1_interface,
                        wl_resource_get_version (factory_resource),
                        new_id);
  wl_resource_set_implementation (surface->surface_synchronization,
                                  &surface_fence_implementation,
                                  surface,
                                  surface_fence_destructor);
}

static const struct zwp_linux_explicit_synchronization_v1_interface factory_implementation =
{
  factory_destroy,
  factory_get_synchronization
};

static void
explicit_sync_bind (struct wl_client *client,
                    void             *data,
                    uint32_t          version,
                    uint32_t          id)
{
  struct wl_resource *resource;

  resource = wl_resource_create (client,
                                 &zwp_linux_explicit_synchronization_v1_interface,
                                 version,
                                 id);
  wl_resource_set_implementation (resource,
                                  &factory_implementation,
                                  NULL,
                                  NULL);
}

void
meta_wayland_explicit_synchronization_init (MetaWaylandCompositor *compositor)
{
  MetaBackend *backend = meta_get_backend ();
  MetaEgl *egl = meta_backend_get_egl (backend);
  ClutterBackend *clutter_backend = meta_backend_get_clutter_backend (backend);
  CoglContext *cogl_context = clutter_backend_get_cogl_context (clutter_backend);
  EGLDisplay egl_display = cogl_egl_context_get_egl_display (cogl_context);

  /*
   * We use:
   *  - EGL_KHR_fence_sync to create fences on rendering
   *  - EGL_KHR_wait_sync to have the GPU wait on fences
   *  - EGL_ANDROID_native_fence_sync to go between EGLSync objects and
   *    dma-fence FDs
   *  - EGL_EXT_image_implicit_sync_control to disable implicit sync on
   *    images imported with an attached fence
   */
  if (!meta_egl_has_extensions (egl, egl_display, NULL,
                                "EGL_KHR_fence_sync",
                                "EGL_KHR_wait_sync",
                                "EGL_ANDROID_native_fence_sync",
                                "EGL_EXT_image_implicit_sync_control",
                                NULL))
    return;

  if (!wl_global_create (compositor->wayland_display,
                         &zwp_linux_explicit_synchronization_v1_interface,
                         META_ZWP_LINUX_EXPLICIT_SYNCHRONIZATION_V1_VERSION,
                         compositor,
                         explicit_sync_bind))
    return;
}

static void
meta_wayland_release_fence_finalize (GObject *object)
{
  MetaWaylandReleaseFence *release_fence = META_WAYLAND_RELEASE_FENCE (object);

  if (release_fence->fence_fd >= 0)
    close (release_fence->fence_fd);

  G_OBJECT_CLASS (meta_wayland_release_fence_parent_class)->finalize (object);
}

void
meta_wayland_release_fence_init (MetaWaylandReleaseFence *release_fence)
{
  release_fence->fence_fd = -1;
}

void
meta_wayland_release_fence_class_init (MetaWaylandReleaseFenceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = meta_wayland_release_fence_finalize;
}

static void
meta_wayland_surface_fence_finalize (GObject *object)
{
  MetaWaylandSurfaceFence *surface_fence = META_WAYLAND_SURFACE_FENCE (object);
  MetaBackend *backend = meta_get_backend ();
  MetaEgl *egl = meta_backend_get_egl (backend);
  ClutterBackend *clutter_backend = meta_backend_get_clutter_backend (backend);
  CoglContext *cogl_context = clutter_backend_get_cogl_context (clutter_backend);
  EGLDisplay egl_display = cogl_egl_context_get_egl_display (cogl_context);

  if (surface_fence->last_release)
    g_object_unref (surface_fence->last_release);

  if (surface_fence->in_fence)
    meta_egl_destroy_sync (egl, egl_display, surface_fence->in_fence, NULL);

  wl_list_init (&surface_fence->link);

  G_OBJECT_CLASS (meta_wayland_surface_fence_parent_class)->finalize (object);
}

void
meta_wayland_surface_fence_init (MetaWaylandSurfaceFence *surface_fence)
{
  wl_list_init (&surface_fence->link);
}

void
meta_wayland_surface_fence_class_init (MetaWaylandSurfaceFenceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = meta_wayland_surface_fence_finalize;
}
