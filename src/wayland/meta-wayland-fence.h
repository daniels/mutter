/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */

/*
 * Copyright (C) 2016 Red Hat Inc.
 * Copyright (C) 2017 Intel Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Written by:
 *     Jonas Ådahl <jadahl@gmail.com>
 *     Daniel Stone <daniels@collabora.com>
 */

#ifndef META_WAYLAND_DMA_FENCE_H
#define META_WAYLAND_DMA_FENCE_H

#include <glib.h>
#include <glib-object.h>

#include "wayland/meta-wayland-types.h"
#include <wayland-client-core.h>

#define META_TYPE_WAYLAND_RELEASE_FENCE (meta_wayland_release_fence_get_type ())
G_DECLARE_FINAL_TYPE (MetaWaylandReleaseFence, meta_wayland_release_fence,
                      META, WAYLAND_RELEASE_FENCE, GObject);

#define META_TYPE_WAYLAND_SURFACE_FENCE (meta_wayland_surface_fence_get_type ())
G_DECLARE_FINAL_TYPE (MetaWaylandSurfaceFence, meta_wayland_surface_fence,
                      META, WAYLAND_SURFACE_FENCE, GObject);

void
meta_wayland_explicit_synchronization_init (MetaWaylandCompositor *compositor);

void meta_wayland_surface_fence_wait (MetaWaylandSurface *surface);

MetaWaylandReleaseFence *meta_wayland_release_fence_get (void);

gboolean meta_wayland_surface_add_release_fence (MetaWaylandSurface *surface,
                                                 struct wl_list *list);

void meta_wayland_surface_set_release_fence (struct wl_list *link,
                                             MetaWaylandReleaseFence *release);

void meta_wayland_surface_fence_release (MetaWaylandSurface *surface);

#endif /* META_WAYLAND_DMA_BUF_H */
